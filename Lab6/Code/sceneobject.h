#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <QVector3D>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QOpenGLTexture>

class SceneObject
{
public:
    SceneObject();
    SceneObject(QVector3D location, double scale, QOpenGLTexture* texture, QQuaternion rotation, QVector3D orbit_center, double orbit_speed, QVector3D orbit_normal);

    void init();

    void update();

    QVector3D location;
    double scale;
    QOpenGLTexture* texture;

    QQuaternion rotation;
    QQuaternion rotation_offset;

    QVector3D orbit_center;
    double orbit_speed;
    QQuaternion orbit_position;
    QVector3D orbit_normal;
};

#endif // SCENEOBJECT_H
