//
//  Framework for a raytracer
//  File: sphere.h
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Maarten Everts
//    Jasper van de Gronde
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#ifndef SPHERE_H_115209AE
#define SPHERE_H_115209AE

#include "object.h"

class Sphere : public Object
{
public:
    Sphere(Point position,double r, Vector radius_vector, double angle) : position(position), r(r), radius_vector(radius_vector), angle(angle) { }

    virtual Hit intersect(const Ray &ray);
    virtual Color getTextureColor(const Point &hit);
    virtual Point rotatePoint(const Point &point);

    const Point position;
    const double r;
    const Vector radius_vector;
    const double angle;
};

#endif /* end of include guard: SPHERE_H_115209AE */
