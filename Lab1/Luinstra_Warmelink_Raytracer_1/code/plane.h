//
//  Framework for a raytracer
//  File: triangle.h
//
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#ifndef PLANE_H
#define PLANE_H

#include "object.h"

class Plane : public Object
{
public:
    Plane(Point offset,Vector normal) : offset(offset), normal(normal) { }

    virtual Hit intersect(const Ray &ray);

    const Point offset;
    const Vector normal;
};

#endif /* end of include guard: PLANE_H */
