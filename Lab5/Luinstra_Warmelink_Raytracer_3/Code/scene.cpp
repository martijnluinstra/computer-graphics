//
//  Framework for a raytracer
//  File: scene.cpp
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Maarten Everts
//    Jasper van de Gronde
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "scene.h"
#include "material.h"


Color Scene::trace(const Ray &ray, int depth)
{
    /* Base case of recursion */
    if(depth >= maxRecursionDepth){
        return Color();
    }
    // Find hit object and distance
    Hit min_hit(std::numeric_limits<double>::infinity(),Vector());
    Object *obj = NULL;
    for (unsigned int i = 0; i < objects.size(); ++i) {
        Hit hit(objects[i]->intersect(ray));
        if (hit.t<min_hit.t) {
            min_hit = hit;
            obj = objects[i];
        }
    }

    // No hit? Return background color.
    if (!obj) return Color(0.0, 0.0, 0.0);

    Material *material = obj->material;            //the hit objects material
    Point hit = ray.at(min_hit.t);                 //the hit point
    Vector N = min_hit.N;                          //the normal at hit point
    Vector V = -ray.D;                             //the view vector
    Color material_color;

    // Color is either material color or texture color
    if(material->texture != NULL){
        material_color = obj->getTextureColor(hit);
    } else {
        material_color = material->color;
    }

    /****************************************************
    * This is where you should insert the color
    * calculation (Phong model).
    *
    * Given: material, hit, N, V, lights[]
    * Sought: color
    *
    * Hints: (see triple.h)
    *        Triple.dot(Vector) dot product 
    *        Vector+Vector      vector sum
    *        Vector-Vector      vector difference
    *        Point-Point        yields vector
    *        Vector.normalize() normalizes vector, returns length
    *        double*Color        scales each color component (r,g,b)
    *        Color*Color        dito
    *        pow(a,b)           a to the power of b
    ****************************************************/       

    if(renderMode == RENDER_MODE_PHONG){
        // Render using Phong

        // Initialize each component with the background color.
        Color diffuse = Color(0.0, 0.0, 0.0);
        Color specular = Color(0.0, 0.0, 0.0);

        // Determine the color for each light
        for (unsigned int i = 0; i < lights.size(); ++i){
            // For the ambient component, we need to sum the colors of the lights.

            Vector L = (lights[i]->position - hit).normalized();

            if(shadows){
                /* Check for every object whether it is between hit and the current 
                 * light. If we found any object, we are in it's shadow.
                 */
                Ray shadow_ray(hit,L);
                bool hit_found = false;
                for (unsigned int i = 0; i < objects.size(); ++i) {
                    Hit shadow_hit(objects[i]->intersect(shadow_ray));
                    if(shadow_hit.t == shadow_hit.t){
                        // found a hit!
                        hit_found = true;
                        break;
                    }
                }
                if(hit_found){
                    /* We found a hit, skip the calculation of diffuse and specular 
                     * components and go to the next light (if any).
                     */
                    continue;
                }
            }

            /* Calculate diffuse color, using the direction of the light (L).
             * If the light is behind the object, the diffuse color is black
             */
            diffuse += max(0.0, L.dot(N)) * material_color * lights[i]->color * material->kd;
            
            /* Calculate specular color, using the reflection.
             * If the light is behind the object, the specular color is black.
             */
            specular += pow(max(0.0,(2*N*L.dot(N) - L).dot(V)), material->n) * lights[i]->color * material->ks;
            
        }

        /* Recursively trace a ray that reflects from the angle of view. Add the color it
         * finds to the specular component.
         */
        Ray reflection_ray(hit, (2*N*V.dot(N) - V).normalized());
        Color reflection_color = trace(reflection_ray, depth+1);
        specular += reflection_color * material->ks;

        // calculate the ambient color based on the colors of the lights
        Color ambient = material_color * material->ka;

        return ambient + diffuse + specular;
    }else if(renderMode == RENDER_MODE_NORMAL){
        /* Render using Normal
         * Convert the components of the normal (which are between -1 and 1)
         * to a value between 0 and 1, corresponding to the r, g and b component of the Colors
         */
        Color color = (N+1) / 2;
        return color;
    }else if(renderMode == RENDER_MODE_GOOCH){
        Color specular = Color(0.0, 0.0, 0.0);
        Color diffuse = Color(0.0, 0.0, 0.0);
        Color kBlue = Color(0,0,1);
        Color kYellow = Color(1,1,0);

        for (unsigned int i = 0; i < lights.size(); ++i){
            Vector L = (lights[i]->position - hit).normalized();

            // Calculate material diffuse lighting factor
            Color kd = lights[i]->color * material->color * material->kd;

            // Calculate cool and warm components
            Color k_cool = (gooch->b * kBlue)  + (gooch->alpha * kd);
            Color k_warm = (gooch->y * kYellow) + (gooch->beta * kd);

            // Calculate diffuse and specular component
            diffuse += (k_cool * (1 - N.dot(L))/2) + (k_warm * (1 + N.dot(L))/2);
            specular += pow(max(0.0,(2*N*L.dot(N) - L).dot(V)), material->n) * lights[i]->color * material->ks;
        }
        return diffuse + specular;
    }
    // Return the material color in case of unknown render mode.
    return material->color;
}

void Scene::renderZBuffer(Image &img){
    int w = img.width();
    int h = img.height();

    double zbuffer[w][h];

    // initialize zbuffer

    double zmin = std::numeric_limits<double>::infinity();
    double zmax = 0;

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            zbuffer[x][y] = std::numeric_limits<double>::infinity();
        }
    }

    // Raytrace for zbuffer (safe distances in the buffer)

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Point pixel(x+0.5, h-1-y+0.5, 0);
            Ray ray(camera->eye, (pixel-camera->eye).normalized());
            for (unsigned int i = 0; i < objects.size(); ++i) {
                Hit hit(objects[i]->intersect(ray));
                double distance = (ray.at(hit.t) - camera->eye).length();

                /* Update buffer if this object is closer to the eye 
                 * than anything we have seen before.
                 */
                if (distance < zbuffer[x][y]){
                    zbuffer[x][y] = distance;
                }
                // If needed, update min and max
                if(zmax < distance){
                    zmax = distance;
                }
                if(distance < zmin){
                    zmin = distance;
                }
            }
        }
    }

    // Map distances to colors

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Color color;
            // the background is black
            if(zbuffer[x][y] == std::numeric_limits<double>::infinity()){
                color.set(0.0);
            }else{
                /* map all distances to a range of 0.1:1
                 * we want the most distant object to be distinguishable, 
                 * so we leave a gap between that and the background)
                 */
                color.set(1 - 0.9 * (zbuffer[x][y] - zmin)/ (zmax - zmin));
            }
            img(x,y) = color;
        }
    }
}

void Scene::render(Image &img)
{
    // Use custom render function if RenderMode = ZBuffer
    if(renderMode == RENDER_MODE_ZBUFFER){
        renderZBuffer(img);
        return;
    }

    int w = img.width();
    int h = img.height();

    /* Calculate additional camera settings */
    Vector direction = (camera->center - camera->eye).normalized();
    Vector right = - camera->up.cross(direction);
    
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Color col(0.0, 0.0, 0.0);
            /* Devide pixels in to superSamplingFactor^2 subpixels and average their 
             * color for anti-aliasing
             */
            for(int zx = 0; zx<superSamplingFactor; zx++){
                for(int zy = 0; zy<superSamplingFactor; zy++){
                    double subOffsetX = zx/ (double) superSamplingFactor;
                    double subOffsetY = zy/ (double) superSamplingFactor;

                    double offsetX = x + subOffsetX + subOffsetX/2.0 - w/2.0;
                    double offsetY = y + subOffsetY + subOffsetY/2.0 - h/2.0;

                    /* Create ray based on camera settings. */
                    Point pixel = camera->center + (offsetX - 1)*right + (1 - offsetY)*camera->up;
                    Ray ray(camera->eye, (pixel-camera->eye).normalized());
                    col += trace(ray, 0);
                }
            }
            col /= superSamplingFactor*superSamplingFactor;
            col.clamp();
            img(x,y) = col;
        }
    }
}

void Scene::addObject(Object *o)
{
    objects.push_back(o);
}

void Scene::addLight(Light *l)
{
    lights.push_back(l);
}

void Scene::setEye(Triple e)
{
    /* Legacy, to support yaml files that only specify an eye. 
     * It uses the previously assumed defaults for unknown settings. 
     */
    camera = new Camera();
    camera->eye = e;
    Point *center = new Point(e.x, e.y, 0);
    camera->center = *center;
    Point *up = new Point(0, 1, 0);
    camera->up = *up;
    camera->width = 400;
    camera->height = 400;
}

void Scene::setRenderMode(std::string r)
{
    if(r == "normal"){
        renderMode = RENDER_MODE_NORMAL;
    }else if(r == "zbuffer"){
        renderMode = RENDER_MODE_ZBUFFER;
    }else if(r == "gooch"){
        renderMode = RENDER_MODE_GOOCH;
    }else{
        renderMode = RENDER_MODE_PHONG;
    }
}
