#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "openglwindow.h"
#include "sceneobject.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLFramebufferObject>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLTexture>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QTimer>

// Math includes
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QQuaternion>


#include "objmodel.h"

/* MainWindow is the window used to display the application
 * Do all your changes to the code in this class (and the mainwindow.cpp file)
 */
class MainWindow : public OpenGLWindow
{
public:
    MainWindow();
    ~MainWindow();
    void initialize() Q_DECL_OVERRIDE;
    void render() Q_DECL_OVERRIDE;

public slots:
    void update();

protected:
    // Functions for keyboard input events
    void keyPressEvent(QKeyEvent * ev);
    void keyReleaseEvent(QKeyEvent * ev);

    // Function for mouse input events
    void mouseDoubleClickEvent(QMouseEvent * ev);
    void mouseMoveEvent(QMouseEvent * ev);
    void mousePressEvent(QMouseEvent * ev);
    void mouseReleaseEvent(QMouseEvent * ev);
    void wheelEvent(QWheelEvent * ev);
    void resizeEvent(QResizeEvent * ev);

private:
    void renderObject(SceneObject obj);
    void renderSphere(QVector3D pos, QVector3D color, QVector4D material, QVector3D lightpos);
    void renderRaytracerScene();
    void renderScene();
    void initScene();
    QVector<QVector3D> generateColors(int numberOfColors);

    // Declare your VAO and VBO variables here
    QOpenGLVertexArrayObject vao;
    QOpenGLVertexArrayObject m_sqVAO;

    QOpenGLTexture *textures[5];

    QOpenGLBuffer *coordinatePointer;
    QOpenGLBuffer *colorPointer;
    QOpenGLBuffer *normalPointer;
    QOpenGLBuffer *textureCoordinateBuffer;
    QOpenGLBuffer *m_sqVBO;
    QOpenGLFramebufferObject *m_frameBuffer0;
    QOpenGLFramebufferObject *m_frameBuffer1;

    QMatrix3x3 normalMatrix;

    // Members for the shader and uniform variables
    QOpenGLShaderProgram *m_shaderProgram;
    QOpenGLShaderProgram *b_shaderProgram;

    // Declare other private variables here if you need them
    int numberOfVertices;

    QMatrix4x4 modelMatrix;
    QMatrix4x4 viewMatrix;
    QMatrix4x4 projectionMatrix;

    double scaling;
    bool rotating;
    float apperture;
    float focusPlane;
    QQuaternion rotation;
    QPoint mousePos;

    GLuint  m_MaterialColorUniform ;
    GLuint  m_ModelMatrixUniform ;

    QTimer *timer;

    QVector<SceneObject> scene;
    QVector3D scene_center;
    QVector3D camera_pos;
};

#endif // MAINWINDOW_H
