//
//  Framework for a raytracer
//  File: scene.cpp
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Maarten Everts
//    Jasper van de Gronde
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "scene.h"
#include "material.h"


Color Scene::trace(const Ray &ray)
{
    // Find hit object and distance
    Hit min_hit(std::numeric_limits<double>::infinity(),Vector());
    Object *obj = NULL;
    for (unsigned int i = 0; i < objects.size(); ++i) {
        Hit hit(objects[i]->intersect(ray));
        if (hit.t<min_hit.t) {
            min_hit = hit;
            obj = objects[i];
        }
    }

    // No hit? Return background color.
    if (!obj) return Color(0.0, 0.0, 0.0);

    Material *material = obj->material;            //the hit objects material
    Point hit = ray.at(min_hit.t);                 //the hit point
    Vector N = min_hit.N;                          //the normal at hit point
    Vector V = -ray.D;                             //the view vector


    /****************************************************
    * This is where you should insert the color
    * calculation (Phong model).
    *
    * Given: material, hit, N, V, lights[]
    * Sought: color
    *
    * Hints: (see triple.h)
    *        Triple.dot(Vector) dot product 
    *        Vector+Vector      vector sum
    *        Vector-Vector      vector difference
    *        Point-Point        yields vector
    *        Vector.normalize() normalizes vector, returns length
    *        double*Color        scales each color component (r,g,b)
    *        Color*Color        dito
    *        pow(a,b)           a to the power of b
    ****************************************************/       

    if(renderMode == RENDER_MODE_PHONG){
        // Render using Phong

        // Initialize each component with the background color.
        Color ambient = Color(0.0, 0.0, 0.0);
        Color diffuse = Color(0.0, 0.0, 0.0);
        Color specular = Color(0.0, 0.0, 0.0);

        // Determine the color for each light
        for (unsigned int i = 0; i < lights.size(); ++i){
            // For the ambient component, we need to sum the colors of the lights.
            ambient += lights[i]->color;

            /* Calculate diffuse color, using the direction of the light (L).
             * If the light is behind the object, the diffuse color is black
             */
            Vector L = (lights[i]->position - hit).normalized();
            diffuse += max(0.0, L.dot(N)) * material->color * lights[i]->color * material->kd;
            
            /* Calculate specular color, using the reflection.
             * If the light is behind the object, the specular color is black.
             */
            specular += pow(max(0.0,(2*N*L.dot(N) - L).dot(V)), material->n) * lights[i]->color * material->ks;
        }

        // calculate the ambient color based on the colors of the lights
        ambient = ambient * material->color * material->ka;

        return ambient + diffuse + specular;
    }else if(renderMode == RENDER_MODE_NORMAL){
        /* Render using Normal
         * Convert the components of the normal (which are between -1 and 1)
         * to a value between 0 and 1, corresponding to the r, g and b component of the Colors
         */
        Color color = (N+1) / 2;
        return color;
    }
    // Return the material color in case of unknown render mode.
    return material->color;
}

void Scene::renderZBuffer(Image &img){
    int w = img.width();
    int h = img.height();

    double zbuffer[w][h];

    // initialize zbuffer

    double zmin = std::numeric_limits<double>::infinity();
    double zmax = 0;

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            zbuffer[x][y] = std::numeric_limits<double>::infinity();
        }
    }

    // Raytrace for zbuffer (safe distances in the buffer)

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Point pixel(x+0.5, h-1-y+0.5, 0);
            Ray ray(eye, (pixel-eye).normalized());
            for (unsigned int i = 0; i < objects.size(); ++i) {
                Hit hit(objects[i]->intersect(ray));
                double distance = (ray.at(hit.t) - eye).length();

                /* Update buffer if this object is closer to the eye 
                 * than anything we have seen before.
                 */
                if (distance < zbuffer[x][y]){
                    zbuffer[x][y] = distance;
                }
                // If needed, update min and max
                if(zmax < distance){
                    zmax = distance;
                }
                if(distance < zmin){
                    zmin = distance;
                }
            }
        }
    }

    // Map distances to colors

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Color color;
            // the background is black
            if(zbuffer[x][y] == std::numeric_limits<double>::infinity()){
                color.set(0.0);
            }else{
                /* map all distances to a range of 0.1:1
                 * we want the most distant object to be distinguishable, 
                 * so we leave a gap between that and the background)
                 */
                color.set(1 - 0.9 * (zbuffer[x][y] - zmin)/ (zmax - zmin));
            }
            img(x,y) = color;
        }
    }
}

void Scene::render(Image &img)
{
    // Use custom render function if RenderMode = ZBuffer
    if(renderMode == RENDER_MODE_ZBUFFER){
        renderZBuffer(img);
        return;
    }

    int w = img.width();
    int h = img.height();
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Point pixel(x+0.5, h-1-y+0.5, 0);
            Ray ray(eye, (pixel-eye).normalized());
            Color col = trace(ray);
            col.clamp();
            img(x,y) = col;
        }
    }
}

void Scene::addObject(Object *o)
{
    objects.push_back(o);
}

void Scene::addLight(Light *l)
{
    lights.push_back(l);
}

void Scene::setEye(Triple e)
{
    eye = e;
}

void Scene::setRenderMode(std::string r){
    if(r == "normal"){
        renderMode = RENDER_MODE_NORMAL;
    }else if(r == "zbuffer"){
        renderMode = RENDER_MODE_ZBUFFER;
    }else{
        renderMode = RENDER_MODE_PHONG;
    }
}
