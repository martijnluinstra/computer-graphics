#include "mainwindow.h"

#include <QDebug>
#include <QScreen>
#include <QVector3D>

MainWindow::MainWindow()
    : m_shaderProgram(0)
{
    qDebug() << "Constructor of MainWindow";

    // NOTE: OpenGL functions are not yet enabled when the constructor is called
}

MainWindow::~MainWindow()
{
    qDebug() << "Desctructor of MainWindow";

    // Free all your used resources here

    // Destroy buffers first, before destroying the VertexArrayObject
    coordinatePointer->destroy();
    colorPointer->destroy();
    textureCoordinateBuffer->destroy();
    delete coordinatePointer;
    delete colorPointer;
    delete textureCoordinateBuffer;
    vao.destroy();

    // clean textures
    for (int i = 0; i < 5; ++i)
        delete textures[i];

    // free the pointer of the shading program
    delete m_shaderProgram;

}

// Initialize all your OpenGL objects here
void MainWindow::initialize()
{
    qDebug() << "MainWindow::initialize()";
    QString glVersion;
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    qDebug() << "Using OpenGL" << qPrintable(glVersion);

    // Initialize the shaders
    m_shaderProgram = new QOpenGLShaderProgram(this);
    // Use the ":" to load from the resources files (i.e. from the resources.qrc)
    m_shaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vertex.glsl");
    m_shaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fragment.glsl");
    m_shaderProgram->link();

    // Shaders are initialized
    // You can retrieve the locations of the uniforms from here

    m_MaterialColorUniform = m_shaderProgram->uniformLocation("materialColor");
    m_ModelMatrixUniform = m_shaderProgram->uniformLocation("modelMatrix");


    // Initialize your objects and buffers

    OBJModel cube = OBJModel(":/models/sphere.obj");

    // Create your Vertex Array Object (VAO) and Vertex Buffer Objects (VBO) here.

    QVector<QVector3D> vertices = cube.vertices;

    numberOfVertices = cube.vertices.length();

    QVector<QVector3D> colors = generateColors(cube.indices.length());

    vao.create();
    vao.bind();

    coordinatePointer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    coordinatePointer->create();
    coordinatePointer->bind();

    coordinatePointer->allocate(vertices.data(), numberOfVertices * sizeof(QVector3D));

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    colorPointer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    colorPointer->create();
    colorPointer->bind();

    colorPointer->allocate(colors.data(), colors.length() * sizeof(QVector3D));

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);


    textureCoordinateBuffer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    textureCoordinateBuffer->create();
    textureCoordinateBuffer->bind();

    textureCoordinateBuffer->allocate(cube.texcoords.data(), cube.texcoords.length() * sizeof(QVector2D));

    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(3);

    // Load 5 textures

    QString texture_names[] = {":/textures/planet1.png", ":/textures/planet2.png",":/textures/planet3.png", ":/textures/planet4.png", ":/textures/planet5.png"};

    // Create a texture object for every texture
    for (int i = 0; i < 5; ++i)
    {
        textures[i] = new QOpenGLTexture (QOpenGLTexture::Target2D);
        textures[i]->setWrapMode(QOpenGLTexture::ClampToEdge);
        textures[i]->setMinMagFilters(QOpenGLTexture::LinearMipMapLinear, QOpenGLTexture::Linear);
        textures[i]->setData(QImage(QString(texture_names[i])).mirrored());
    }

    vao.release();

    // Set OpenGL to use Filled triangles (can be used to render points, or lines)
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    // Enable Z-buffering
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    qDebug() << "Depth Buffer size:" <<  this->format().depthBufferSize() << "bits";

    // Function for culling, removing faces which don't face the camera
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);

    // Set the clear color to be black (color used for resetting the screen)
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    initScene();

    // Initiate user interaction parameters
    scaling = 1;
    rotating = false;

    // Initiate QTimer to animate
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    int framerate = 60;
    timer->setInterval(1000/framerate);
    timer->start();
}

void MainWindow::initScene()
{
    scene_center = QVector3D(0, 0, -100);

    // Create a happy star to orbit around
    scene.append(SceneObject(scene_center, 0.3, textures[3], QQuaternion(0.005, 0, 1, 0), scene_center, 0, QVector3D(0, 0, 0)));
    SceneObject star = scene.last();

    // Create some cute little planets with arbitrary properties to generate a nice looking animation
    scene.append(SceneObject(QVector3D(0,0,-80), 0.05, textures[1], QQuaternion(0.01, 0, 0, 1), star.location, -0.005, QVector3D(0, 1, 0)));
    scene.append(SceneObject(QVector3D(0,0,-50), 0.16, textures[4], QQuaternion(0.005, 1, 0, 0),  star.location, -0.006, QVector3D(.25, .75, 0).normalized() ));
    scene.append(SceneObject(QVector3D(0,0,-60),  0.08, textures[2], QQuaternion(0.008, 0.316228, 0.948683, 0), star.location, 0.015 , QVector3D(.1, .9, 0).normalized() ));
    scene.append(SceneObject(QVector3D(0,0,-30),  0.12, textures[0], QQuaternion(-0.005, 1, 0, 0), star.location, 0.009 , QVector3D(-.5, .8, 0).normalized() ));
}

QVector<QVector3D> MainWindow::generateColors(int numberOfColors){
    QVector<QVector3D> colors;

    srand (42);

    // assign random colors to triangles
    for(int i = 0; i < numberOfColors; i++){
        double r, g, b;
        r = (double) rand() / RAND_MAX;
        g = (double) rand() / RAND_MAX;
        b = (double) rand() / RAND_MAX;
        colors.append(QVector3D(r,g,b));
        colors.append(QVector3D(r,g,b));
        colors.append(QVector3D(r,g,b));
    }
    return colors;
}

void MainWindow::renderObject(SceneObject obj)
{
    // Transform the object

    modelMatrix.setToIdentity();
    modelMatrix.translate(obj.location);
    modelMatrix.rotate(obj.rotation_offset.normalized());
    modelMatrix.scale(obj.scale);

    // Bind the correct texture for the object
    obj.texture->bind(GL_TEXTURE0);

    // Render the object
    m_shaderProgram->setUniformValue(m_ModelMatrixUniform, modelMatrix);
    glDrawArrays(GL_TRIANGLES, 0, numberOfVertices);

}

void MainWindow::renderScene()
{
    // Update and render all objects in the scene
    for (int i = 0; i < scene.length(); i++)
    {
        scene[i].update();
        renderObject(scene[i]);
    }
}

void MainWindow::update(){
    // Restart timer and render scene.
    timer->start();
    renderLater();
}

// The render function, called when an update is requested
void MainWindow::render()
{
    // glViewport is used for specifying the resolution to render
    // Uses the window size as the resolution
    const qreal retinaScale = devicePixelRatio();
    glViewport(0, 0, width() * retinaScale, height() * retinaScale);

    // Clear the screen at the start of the rendering.
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Bind the shaderprogram to use it
    m_shaderProgram->bind();

    vao.bind();

    // Rendering can be done here
    // Any transformation you whish to do or setting a uniform
    // should be done before any call to glDraw*

    // set all matrices to Identity
    modelMatrix.setToIdentity();
    viewMatrix.setToIdentity();
    projectionMatrix.setToIdentity();

    // Translate, rotate and scale view
    viewMatrix.translate(scene_center);
    viewMatrix.rotate(rotation.normalized());
    viewMatrix.scale(scaling);
    viewMatrix.translate(-scene_center);

    // Set projection
    projectionMatrix.perspective(60, width()/(double) height(), 1, 1000);

    m_shaderProgram->setUniformValue("viewMatrix", viewMatrix);
    m_shaderProgram->setUniformValue("projectionMatrix", projectionMatrix);

    renderScene();

    // relases the current shaderprogram (to bind an use another shaderprogram for example)
    m_shaderProgram->release();

}


// Below are functions which are triggered by input events:

// Triggered by pressing a key
void MainWindow::keyPressEvent(QKeyEvent *ev)
{
    switch(ev->key()) {
    case 'A': qDebug() << "A pressed"; break;
    default:
        // ev->key() is an integer. For alpha numeric characters keys it equivalent with the char value ('A' == 65, '1' == 49)
        // Alternatively, you could use Qt Key enums, see http://doc.qt.io/qt-5/qt.html#Key-enum
        qDebug() << ev->key() << "pressed";
        break;
    }

    // Used to update the screen
    renderLater();
}

// Triggered by releasing a key
void MainWindow::keyReleaseEvent(QKeyEvent *ev)
{
    switch(ev->key()) {
    case 'A': qDebug() << "A released"; break;
    default:
        qDebug() << ev->key() << "released";
        break;
    }

    renderLater();
}

// Triggered by clicking two subsequent times on any mouse button.
void MainWindow::mouseDoubleClickEvent(QMouseEvent *ev)
{
    qDebug() << "Mouse double clicked:" << ev->button();

    renderLater();
}

// Triggered when moving the mouse inside the window (even when no mouse button is clicked!)
void MainWindow::mouseMoveEvent(QMouseEvent *ev)
{
    if(rotating){
        QPoint curPos = ev->pos();

        // calculate rotation angle
        double xfactor = (mousePos.x() - curPos.x()) / (double) width();
        double yfactor = (mousePos.y() - curPos.y()) / (double) height();

        // apply angles
        QQuaternion* xmod = new QQuaternion((0.5 * xfactor), 0,1,0);
        QQuaternion* ymod = new QQuaternion((0.5 * yfactor), 1,0,0);
        rotation *= *xmod * *xmod;
        rotation *= *ymod * *ymod;

        // update mousePos
        mousePos = curPos;

    }
   renderLater();
}

// Triggered when pressing any mouse button
void MainWindow::mousePressEvent(QMouseEvent *ev)
{
    // set rotation

    mousePos = ev->pos();
    rotating = true;

    renderLater();
}

// Triggered when releasing any mouse button
void MainWindow::mouseReleaseEvent(QMouseEvent *ev)
{
    // unset rotation
    mousePos = ev->pos();
    rotating = false;
}

// Triggered when clicking scrolling with the scroll wheel on the mouse
void MainWindow::wheelEvent(QWheelEvent * ev)
{
    // scale (factor 2 to make it smoother).
    double scale = 2.0/ev->delta();
    scaling *= 1-scale;

    renderLater();
}
