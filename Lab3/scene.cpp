//
//  Framework for a raytracer
//  File: scene.cpp
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Maarten Everts
//    Jasper van de Gronde
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "scene.h"
#include "material.h"


Color Scene::trace(const Ray &ray, int depth)
{
    if(depth >= maxRecursionDepth){
        return Color();
    }
    // Find hit object and distance
    Hit min_hit(std::numeric_limits<double>::infinity(),Vector());
    Object *obj = NULL;
    for (unsigned int i = 0; i < objects.size(); ++i) {
        Hit hit(objects[i]->intersect(ray));
        if (hit.t<min_hit.t) {
            min_hit = hit;
            obj = objects[i];
        }
    }

    // No hit? Return background color.
    if (!obj) return Color(0.0, 0.0, 0.0);

    Material *material = obj->material;            //the hit objects material
    Point hit = ray.at(min_hit.t);                 //the hit point
    Vector N = min_hit.N;                          //the normal at hit point
    Vector V = -ray.D;                             //the view vector


    /****************************************************
    * This is where you should insert the color
    * calculation (Phong model).
    *
    * Given: material, hit, N, V, lights[]
    * Sought: color
    *
    * Hints: (see triple.h)
    *        Triple.dot(Vector) dot product 
    *        Vector+Vector      vector sum
    *        Vector-Vector      vector difference
    *        Point-Point        yields vector
    *        Vector.normalize() normalizes vector, returns length
    *        double*Color        scales each color component (r,g,b)
    *        Color*Color        dito
    *        pow(a,b)           a to the power of b
    ****************************************************/       

    if(renderMode == RENDER_MODE_PHONG){
        // Render using Phong

        // Initialize each component with the background color.
        Color diffuse = Color(0.0, 0.0, 0.0);
        Color specular = Color(0.0, 0.0, 0.0);

        // Determine the color for each light
        for (unsigned int i = 0; i < lights.size(); ++i){
            // For the ambient component, we need to sum the colors of the lights.

            Vector L = (lights[i]->position - hit).normalized();

            if(shadows){
                Ray shadow_ray(hit,L);
                bool hit_found = false;
                for (unsigned int i = 0; i < objects.size(); ++i) {
                    Hit shadow_hit(objects[i]->intersect(shadow_ray));
                    if(shadow_hit.t == shadow_hit.t){
                        // found a hit!
                        hit_found = true;
                        break;
                    }
                }
                if(hit_found){
                    continue;
                }
            }

            /* Calculate diffuse color, using the direction of the light (L).
             * If the light is behind the object, the diffuse color is black
             */
            diffuse += max(0.0, L.dot(N)) * material->color * lights[i]->color * material->kd;
            
            /* Calculate specular color, using the reflection.
             * If the light is behind the object, the specular color is black.
             */
            specular += pow(max(0.0,(2*N*L.dot(N) - L).dot(V)), material->n) * lights[i]->color * material->ks;
            
        }

        Ray reflection_ray(hit, (2*N*V.dot(N) - V).normalized());
        Color reflection_color = trace(reflection_ray, depth+1);
        specular += reflection_color * material->ks;

        // calculate the ambient color based on the colors of the lights
        Color ambient = material->color * material->ka;

        return ambient + diffuse + specular;
    }else if(renderMode == RENDER_MODE_NORMAL){
        /* Render using Normal
         * Convert the components of the normal (which are between -1 and 1)
         * to a value between 0 and 1, corresponding to the r, g and b component of the Colors
         */
        Color color = (N+1) / 2;
        return color;
    }
    // Return the material color in case of unknown render mode.
    return material->color;
}

void Scene::renderCamera(Image &img){
    Camera cam = getCamera();
    int w = img.width();
    int h = img.height();
    
    
    /*
      Triple cameraDirection = (cam.reference - cam.position);
      Triple cameraGaze = cameraDirection.length();
      Triple upVector = cam.upVector;
      
      Triple AVec = cameraDirection.cross(upVector);
      triple BVec = AVec.cross(cameraPosition);
      triple MVec = camera.position + cameraDirection;
      triple HVec = (AVec * cameraGaze * tan(phi)) / AVec.length();
      triple VVec = (BVec * cameraGaze * tan(theta)) / BVec.length();
      Point p = MVec + (2*w - 1) * HVec + (2*h - 1) * VVec;
      Triple PE = (p - cam.position).normalized();
      Ray ray(cam.position + PE* */
      
      
    //Triple cameraUp = cam.upVector;
    //Triple cameraRight = cameraDirection.cross(cameraUp);
    //cameraUp = cameraRight.cross(cameraDirection);
    
//      cam.position = cam.position + cameraDirection;
      //
      //cam2.normalize();
      //cam.position = cam.position * (cam2);// + cam.position;
      //Triple cam2 = cam.reference + 0*w + 0.75*h;
      //cam.position = cam2+cam.position;
      
    // Start supersampling process. 
    for (int y = 0; y < h; y++) {
	for (int x = 0; x < w; x++) {
	    Color col(0.0, 0.0, 0.0);
	    for(int zx = 0; zx<superSamplingFactor; zx++){
		for(int zy = 0; zy<superSamplingFactor; zy++){
		    double subOffsetX = zx/ (double) superSamplingFactor;
		    double subOffsetY = zy/ (double) superSamplingFactor;
		    Point pixel(x+ subOffsetX + subOffsetX/2.0, h-1-y + subOffsetY + subOffsetY/2.0, 0);
		    Ray ray(cam.position, (pixel-cam.position).normalized());
		    col += trace(ray, 0);
		}
	    }
	    col /= superSamplingFactor*superSamplingFactor;
	    col.clamp();
	    img(x,y) = col;
	}
    }
}


void Scene::renderZBuffer(Image &img){
    int w = img.width();
    int h = img.height();

    double zbuffer[w][h];

    // initialize zbuffer

    double zmin = std::numeric_limits<double>::infinity();
    double zmax = 0;

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            zbuffer[x][y] = std::numeric_limits<double>::infinity();
        }
    }

    // Raytrace for zbuffer (safe distances in the buffer)

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Point pixel(x+0.5, h-1-y+0.5, 0);
            Ray ray(eye, (pixel-eye).normalized());
            for (unsigned int i = 0; i < objects.size(); ++i) {
                Hit hit(objects[i]->intersect(ray));
                double distance = (ray.at(hit.t) - eye).length();

                /* Update buffer if this object is closer to the eye 
                 * than anything we have seen before.
                 */
                if (distance < zbuffer[x][y]){
                    zbuffer[x][y] = distance;
                }
                // If needed, update min and max
                if(zmax < distance){
                    zmax = distance;
                }
                if(distance < zmin){
                    zmin = distance;
                }
            }
        }
    }

    // Map distances to colors

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Color color;
            // the background is black
            if(zbuffer[x][y] == std::numeric_limits<double>::infinity()){
                color.set(0.0);
            }else{
                /* map all distances to a range of 0.1:1
                 * we want the most distant object to be distinguishable, 
                 * so we leave a gap between that and the background)
                 */
                color.set(1 - 0.9 * (zbuffer[x][y] - zmin)/ (zmax - zmin));
            }
            img(x,y) = color;
        }
    }
}

void Scene::render(Image &img)
{
    // Use custom render function if RenderMode = ZBuffer
    if(renderMode == RENDER_MODE_ZBUFFER){
        renderZBuffer(img);
        return;
    }
    // Render using the Camera render function if a Camera is used instead of an eye 
 
    int w = img.width();
    int h = img.height();
    for (int y = 0; y < h; y++) {
	for (int x = 0; x < w; x++) {
	    Color col(0.0, 0.0, 0.0);
	    for(int zx = 0; zx<superSamplingFactor; zx++){
		for(int zy = 0; zy<superSamplingFactor; zy++){
		    double subOffsetX = zx/ (double) superSamplingFactor;
		    double subOffsetY = zy/ (double) superSamplingFactor;
		    Point pixel(x+ subOffsetX + subOffsetX/2.0, h-1-y + subOffsetY + subOffsetY/2.0, 0);
		    Ray ray(eye, (pixel-eye).normalized());
		    col += trace(ray, 0);
		}
	    }
	    col /= superSamplingFactor*superSamplingFactor;
	    col.clamp();
	    img(x,y) = col;
	}
    }
}

void Scene::addObject(Object *o)
{
    objects.push_back(o);
}

void Scene::addLight(Light *l)
{
    lights.push_back(l);
}

void Scene::setCamera(Camera *c){
    camera = c;
}

Camera Scene::getCamera(){
    return *camera;
}

void Scene::setUsingCamera(bool b){
    usingCamera = b;
}

bool Scene::getUsingCamera(){
    return usingCamera;
}

void Scene::setEye(Triple e)
{
    eye = e;
}

void Scene::setRenderMode(std::string r)
{
    if(r == "normal"){
        renderMode = RENDER_MODE_NORMAL;
    }else if(r == "zbuffer"){
        renderMode = RENDER_MODE_ZBUFFER;
    }else{
        renderMode = RENDER_MODE_PHONG;
    }
}
