#ifndef TRIANGLE_H_INCLUDED
#define TRIANGLE_H_INCLUDED

#include "object.h"

class Triangle : public Object
{
public:
    Triangle(Point vertex1, Point vertex2, Point vertex3) : vertex1(vertex1), vertex2(vertex2), vertex3(vertex3) { }

    virtual Hit intersect(const Ray &ray);
    virtual Color getTextureColor(const Point &hit);
    virtual Point rotatePoint(const Point &point);


    Point vertex1;
    Point vertex2;
    Point vertex3;
};

#endif /* end of include guard: */