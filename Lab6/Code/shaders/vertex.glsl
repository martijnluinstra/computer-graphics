#version 330
// Specify the input locations of attributes
layout(location=0) in vec3 posAttr;     // vertex position
layout(location=1) in vec3 colAttr;     // vertex color color
layout(location=2) in vec3 normalAttr;  // vertex normal
layout(location=3) in vec2 textureAttr;

// Specify the Uniforms of the vertex shaders
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat3 normalMatrix;

// Specify the outputs of the vertex shader
// These will be the input for the fragment shader
out vec2 textureCoords;
out vec3 N;
out vec3 V;

void main()
{
    // gl_Position is the output of the vertex shader
    // Currently without any transformation
    V = vec3(viewMatrix * modelMatrix * vec4(posAttr,1.0));
    N = normalize(normalMatrix * normalAttr);
    textureCoords = textureAttr;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(posAttr,1.0);
}
