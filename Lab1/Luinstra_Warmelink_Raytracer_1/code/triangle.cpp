//
//  Framework for a raytracer
//  File: triangle.cpp
//
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "triangle.h"
#include <iostream>
#include <math.h>

/************************** Triangle **********************************/

Hit Triangle::intersect(const Ray &ray)
{
    Vector N = (v1 - v0).cross(v2-v0).normalized();
    double d = N.dot(v0);
    double t = (N.dot(ray.O) + d) / N.dot(ray.D); 

    if(t<=0){
        return Hit::NO_HIT();
    }

    return Hit(t,N);
}
