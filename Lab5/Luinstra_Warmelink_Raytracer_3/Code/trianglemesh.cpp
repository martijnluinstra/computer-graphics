#include "trianglemesh.h"
#include "triangle.h"
#include <iostream>
#include <math.h>

Hit TriangleMesh::intersect(const Ray &ray)
{
    // check for every triangle whether the ray intersects
    Hit min_hit(std::numeric_limits<double>::infinity(),Vector());
    bool hit_found = false;
    for (unsigned int i = 0; i < triangles.size(); ++i) {
        Hit hit(triangles[i].intersect(ray));
        if (hit.t<min_hit.t) {
            min_hit = hit;
            hit_found = true;
        }
    }

    // return the closest hit
    if (hit_found){
        return min_hit;
    }

    return Hit::NO_HIT();
}

Color TriangleMesh::getTextureColor(const Point &hit)
{
    return material->color;
}

Point TriangleMesh::rotatePoint(const Point &point){
    return point;
}

void TriangleMesh::loadTriangles(GLMmodel* model){
    for (unsigned int i = 0; i < model->numtriangles; ++i) {
        // Read vertices
        int idx0 = model->triangles[i].vindices[0];
        int idx1 = model->triangles[i].vindices[1];
        int idx2 = model->triangles[i].vindices[2];
        Point *v0 = new Point((double) model->vertices[idx0*3], (double) model->vertices[idx0*3+1], (double) model->vertices[idx0*3+2]);
        Point *v1 = new Point((double) model->vertices[idx1*3], (double) model->vertices[idx1*3+1], (double) model->vertices[idx1*3+2]);
        Point *v2 = new Point((double) model->vertices[idx2*3], (double) model->vertices[idx2*3+1], (double) model->vertices[idx2*3+2]);
        
        // Make triangle and translate its position
        triangles.push_back(Triangle(*v0 + position, *v1 + position, *v2 + position));
    }
}
