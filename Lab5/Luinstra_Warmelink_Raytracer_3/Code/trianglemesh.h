#ifndef TRIANGLEMESH_H_INCLUDED
#define TRIANGLEMESH_H_INCLUDED

#include <vector>
#include "object.h"
#include "glm.h"
#include "triangle.h"

class TriangleMesh : public Object
{
public:
    TriangleMesh(Point position) : position(position) { }

    virtual Hit intersect(const Ray &ray);
    virtual Color getTextureColor(const Point &hit);
    virtual Point rotatePoint(const Point &point);
    void loadTriangles(GLMmodel* model);


    const Point position;
    vector<Triangle> triangles;
};

#endif /* end of include guard: */