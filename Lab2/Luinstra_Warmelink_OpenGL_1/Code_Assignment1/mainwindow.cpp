#include "mainwindow.h"

#include <QDebug>
#include <QScreen>
#include <QVector3D>

MainWindow::MainWindow()
    : m_shaderProgram(0)
{
    qDebug() << "Constructor of MainWindow";

    // NOTE: OpenGL functions are not yet enabled when the constructor is called
}

MainWindow::~MainWindow()
{
    qDebug() << "Desctructor of MainWindow";

    // Free all your used resources here

    // Destroy buffers first, before destroying the VertexArrayObject
    coordinatePointer->destroy();
    colorPointer->destroy();
    delete coordinatePointer;
    delete colorPointer;
    vao.destroy();

    // free the pointer of the shading program
    delete m_shaderProgram;

}

// Initialize all your OpenGL objects here
void MainWindow::initialize()
{
    qDebug() << "MainWindow::initialize()";
    QString glVersion;
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));
    qDebug() << "Using OpenGL" << qPrintable(glVersion);

    // Initialize the shaders
    m_shaderProgram = new QOpenGLShaderProgram(this);
    // Use the ":" to load from the resources files (i.e. from the resources.qrc)
    m_shaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vertex.glsl");
    m_shaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fragment.glsl");
    m_shaderProgram->link();

    // Shaders are initialized
    // You can retrieve the locations of the uniforms from here


    // Initialize your objects and buffers

    OBJModel cube = OBJModel(":/models/cube.obj");

    // Create your Vertex Array Object (VAO) and Vertex Buffer Objects (VBO) here.

    QVector<QVector3D> vertices = cube.vertices;

    numberOfVertices = cube.vertices.length();

    QVector<QVector3D> colors = generateColors(cube.indices.length());

    vao.create();
    vao.bind();

    coordinatePointer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    coordinatePointer->create();
    coordinatePointer->bind();

    coordinatePointer->allocate(vertices.data(), numberOfVertices * sizeof(QVector3D));

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    colorPointer = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    colorPointer->create();
    colorPointer->bind();

    colorPointer->allocate(colors.data(), colors.length() * sizeof(QVector3D));

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    vao.release();

    // Set OpenGL to use Filled triangles (can be used to render points, or lines)
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    // Enable Z-buffering
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    qDebug() << "Depth Buffer size:" <<  this->format().depthBufferSize() << "bits";

    // Function for culling, removing faces which don't face the camera
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);

    // Set the clear color to be black (color used for resetting the screen)
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    scaling = 1;
    rotating = false;
}

QVector<QVector3D> MainWindow::generateColors(int numberOfColors){
    QVector<QVector3D> colors;

    srand (42);

    // assign random colors to triangles
    for(int i = 0; i < numberOfColors; i++){
        double r, g, b;
        r = (double) rand() / RAND_MAX;
        g = (double) rand() / RAND_MAX;
        b = (double) rand() / RAND_MAX;
        colors.append(QVector3D(r,g,b));
        colors.append(QVector3D(r,g,b));
        colors.append(QVector3D(r,g,b));
    }
    return colors;
}

void MainWindow::renderSphere(QVector3D pos, QVector3D color, QVector4D material, QVector3D lightpos)
{
    // OpenGL assignment 1, part 2: create a function to render the sphere
    // Use OBJModel(":/models/spehere.obj") for the model

    // you must remove these Q_UNUSED when you implement this function
    Q_UNUSED(pos)
    Q_UNUSED(color)
    Q_UNUSED(material)
    Q_UNUSED(lightpos)
}

/**
 * Renders a similar scene used for the raytracer:
 * 5 colored spheres with a single light
 */
void MainWindow::renderRaytracerScene()
{
    QVector3D lightpos = QVector3D(-200,600,1500);

    // Blue sphere
    renderSphere(QVector3D(90,320,100),QVector3D(0,0,1),QVector4D(0.2f,0.7f,0.5f,64),lightpos);

    // Green sphere
    renderSphere(QVector3D(210,270,300),QVector3D(0,1,0),QVector4D(0.2f,0.3f,0.5f,8),lightpos);

    // Red sphere
    renderSphere(QVector3D(290,170,150),QVector3D(1,0,0),QVector4D(0.2f,0.7f,0.8f,32),lightpos);

    // Yellow sphere
    renderSphere(QVector3D(140,220,400),QVector3D(1,0.8f,0),QVector4D(0.2f,0.8f,0.0f,1),lightpos);

    // Orange sphere
    renderSphere(QVector3D(110,130,200),QVector3D(1,0.5f,0),QVector4D(0.2f,0.8f,0.5f,32),lightpos);
}

// The render function, called when an update is requested
void MainWindow::render()
{
    // glViewport is used for specifying the resolution to render
    // Uses the window size as the resolution
    const qreal retinaScale = devicePixelRatio();
    glViewport(0, 0, width() * retinaScale, height() * retinaScale);

    // Clear the screen at the start of the rendering.
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Bind the shaderprogram to use it
    m_shaderProgram->bind();

    vao.bind();

    // Rendering can be done here
    // Any transformation you whish to do or setting a uniform
    // should be done before any call to glDraw*

    // set all matrices to Identity
    modelMatrix.setToIdentity();
    viewMatrix.setToIdentity();
    projectionMatrix.setToIdentity();

    // Translate, rotate and scale model
    modelMatrix.translate(0, 0, -5);
    modelMatrix.rotate(rotation.normalized());
    modelMatrix.scale(scaling);

    // Set projection
    projectionMatrix.perspective(60, 1, 1, 100);

    m_shaderProgram->setUniformValue("modelMatrix", modelMatrix);
    m_shaderProgram->setUniformValue("viewMatrix", viewMatrix);
    m_shaderProgram->setUniformValue("projectionMatrix", projectionMatrix);

    glDrawArrays(GL_TRIANGLES, 0, numberOfVertices);

    // OpenGl assignment 1, part 2:
    // To render the scene from the raytracer:
    // Make sure your camera is positioned at (200,200,1000) in world coordinates
    // before calling the render function.
    // Make sure that the light position is fixed relative to the world.
    // (You should be able to see the dark side of the scene when rotation)

    // renderRaytracerScene()


    // relases the current shaderprogram (to bind an use another shaderprogram for example)
    m_shaderProgram->release();

}


// Below are functions which are triggered by input events:

// Triggered by pressing a key
void MainWindow::keyPressEvent(QKeyEvent *ev)
{
    switch(ev->key()) {
    case 'A': qDebug() << "A pressed"; break;
    default:
        // ev->key() is an integer. For alpha numeric characters keys it equivalent with the char value ('A' == 65, '1' == 49)
        // Alternatively, you could use Qt Key enums, see http://doc.qt.io/qt-5/qt.html#Key-enum
        qDebug() << ev->key() << "pressed";
        break;
    }

    // Used to update the screen
    renderLater();
}

// Triggered by releasing a key
void MainWindow::keyReleaseEvent(QKeyEvent *ev)
{
    switch(ev->key()) {
    case 'A': qDebug() << "A released"; break;
    default:
        qDebug() << ev->key() << "released";
        break;
    }

    renderLater();
}

// Triggered by clicking two subsequent times on any mouse button.
void MainWindow::mouseDoubleClickEvent(QMouseEvent *ev)
{
    qDebug() << "Mouse double clicked:" << ev->button();

    renderLater();
}

// Triggered when moving the mouse inside the window (even when no mouse button is clicked!)
void MainWindow::mouseMoveEvent(QMouseEvent *ev)
{
    if(rotating){
        QPoint curPos = ev->pos();

        // calculate rotation angle
        double xfactor = (mousePos.x() - curPos.x()) / (double) width();
        double yfactor = (mousePos.y() - curPos.y()) / (double) height();

        // apply angles
        QQuaternion* xmod = new QQuaternion((0.5 * xfactor), 0,1,0);
        QQuaternion* ymod = new QQuaternion((0.5 * yfactor), 1,0,0);
        rotation *= *xmod * *xmod;
        rotation *= *ymod * *ymod;

        // update mousePos
        mousePos = curPos;

    }
   renderLater();
}

// Triggered when pressing any mouse button
void MainWindow::mousePressEvent(QMouseEvent *ev)
{
    // set rotation

    mousePos = ev->pos();
    rotating = true;

    renderLater();
}

// Triggered when releasing any mouse button
void MainWindow::mouseReleaseEvent(QMouseEvent *ev)
{
    // unset rotation
    mousePos = ev->pos();
    rotating = false;
}

// Triggered when clicking scrolling with the scroll wheel on the mouse
void MainWindow::wheelEvent(QWheelEvent * ev)
{
    // scale (factor 2 to make it smoother).
    double scale = 2.0 / (ev->delta());
    scaling *= 1 + scale;

    renderLater();
}
