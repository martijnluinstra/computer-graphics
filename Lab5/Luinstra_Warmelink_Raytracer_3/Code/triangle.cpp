#include "triangle.h"
#include <iostream>
#include <math.h>

/* Triangle intersection algorithm, based on the Möller-Trumbore algorithm, specified 
 * in the paper "Fast Minimum Storage Ray / Triangle Intersection" by Tomas Möller and 
 * Ben Trumbore
 */

Hit Triangle::intersect(const Ray &ray)
{
    double epsilon = 0.000001;

    /* Calculate two edges */
    Triple edge1 = vertex2 - vertex1;
    Triple edge2 = vertex3 - vertex1;

    /* Calculate the Normal vector for the triangle: */
    Vector N = (edge1.cross(edge2)).normalized();

    /* Pvector, used to calculate determinant and U parameter. */
    Triple pvec = ray.D.cross(edge2);

    /* Calculate determinant. */
    double det = edge1.dot(pvec);

    /*  If the determinant is very close to 0, do not count a hit 
     *  eliminates parallel rays) 
     */
    if((det > (epsilon*-1)) && (det < epsilon)){
        return Hit::NO_HIT();
    }

    double inv_det = 1.0/det;
    Triple tvec = ray.O - vertex1;

    /* Calculate u part of barycentric coordinate */
    double u = tvec.dot(pvec) * inv_det;
    if(u < 0.0 || u > 1.0){
        return Hit::NO_HIT();
    }

    Triple qvec = tvec.cross(edge1);

    /* Calculate v part of barycentric coordinate */
    double v = ray.D.dot(qvec) * inv_det;
    if( v < 0.0 || (u + v) > 1.0){
        return Hit::NO_HIT();
    }

    /* calculate distance to hit point */
    double t = (edge2.dot(qvec) ) * inv_det;

    if(t < 0.001){ 
        return Hit::NO_HIT();
    }    
    return Hit(t,N);
}

Color Triangle::getTextureColor(const Point &hit)
{
    return material->color;
}

Point Triangle::rotatePoint(const Point &point){
    return point;
}

