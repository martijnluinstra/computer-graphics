//
//  Framework for a raytracer
//  File: triangle.h
//
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "object.h"

class Triangle : public Object
{
public:
    Triangle(Point v0,Point v1,Point v2) : v0(v0), v1(v1), v2(v2) { }

    virtual Hit intersect(const Ray &ray);

    const Point v0;
    const Point v1;
    const Point v2;
};

#endif /* end of include guard: TRIANGLE_H */
