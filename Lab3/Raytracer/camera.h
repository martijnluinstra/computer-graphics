//
//  Framework for a raytracer
//  File: camera.h
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Martijn Luinstra, Steven Warmelink
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#ifndef CAMERA_H_
#define CAMERA_H_

#include "camera.h"

class Camera
{
public:
	Point eye;
	Point center;
    Vector up;
    int width;
    int height;
};

#endif /* end of include guard: CAMERA_H_ */
