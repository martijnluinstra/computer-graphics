#version 330
// Specify the inputs to the fragment shader
in vec2 textureCoords;
in vec3 N;
in vec3 V;

// Specify the Uniforms of the vertex shaders
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform sampler2D samplerUniform;

uniform vec3 materialColor;
uniform vec4 material;
uniform vec3 lightPos;

uniform bool DOF;
uniform float apperture;
uniform float focusPlane;
uniform vec2 windowSize;

// Specify the output of the fragment shader
// Usually a vec4 describing a color
out vec4 fColor;

void main()
{
    vec2 frustrum = vec2(1,8000);
    // Plain white
    // fColor = texture2D(samplerUniform,textureCoords);
    vec4 color = vec4(materialColor, 1.0);
    vec3 L = normalize(lightPos - V);
    vec3 E = normalize(-V);
    vec3 R = normalize(-reflect(L,N));

    //calculate Ambient Term:
    vec4 Iamb = material[0] * color;

    //calculate Diffuse Term:
    vec4 Idiff = material[1] * max(dot(N,L), 0.0) * color;

    vec4 Ispec = vec4(0,0,0,0);
    // calculate Specular Term:
    if(dot(N,L) > 0.0){
        Ispec = material[2] * pow(max(dot(R,E),0.0), material[3]) * vec4(1,1,1,1);
    }

    // write Total Color:
    fColor = vec4(vec3(Iamb + Idiff + Ispec), 1.0);
    if(DOF){
//        float depth = V.z/-2000.0;
//        fColor = vec4(depth, depth, depth, 1.0);
//        if(V.z > focusPlane + 50 || V.z < focusPlane-50){
//            fColor = texture2D(samplerUniform, gl_FragCoord.xy / windowSize[0]);
//        }
        float weight = min(abs(focusPlane-abs(V.z))*apperture/(length(frustrum)), 1.0);
//        float weight = (abs(V.z - focusPlane) * apperture)/focusPlane;
//        fColor = vec4(weight);
        fColor *= (1-weight);
        fColor += weight * texture2D(samplerUniform, gl_FragCoord.xy / windowSize[0]);
    }
}
