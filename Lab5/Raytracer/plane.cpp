#include "plane.h"
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <cmath>

Hit Plane::intersect(const Ray &ray)
{
  float denom = normal.dot(ray.D);
  double t = (pos - ray.O).dot(normal) / denom;
  if (t >= 0.001){
      return Hit(t,normal);
  }
  else return Hit::NO_HIT();
  
  
  
  /*
  double t = -1 *(normal.dot(ray.O) + pos) / normal.dot(ray.D);
  if(t < 0){
    return Hit::NO_HIT();
  }
  
  Triple pvec = ray.O + t*ray.D;
  
  Triple planeTRS = ray.O - pvec;
  double dot = planeTRS.dot(normal);
  if(dot > 0){
    return Hit::NO_HIT();
  }
  else return Hit(t,normal);
  */
}

Color Plane::getTextureColor(const Point &hit)
{
    return material->color;
}

Point Plane::rotatePoint(const Point &point){
    return point;
}
