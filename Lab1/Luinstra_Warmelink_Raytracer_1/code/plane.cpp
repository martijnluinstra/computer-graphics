//
//  Framework for a raytracer
//  File: triangle.cpp
//
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "plane.h"
#include <iostream>
#include <math.h>

/************************** Plane **********************************/

Hit Plane::intersect(const Ray &ray)
{
	double d = offset.length();
    double t = (ray.O.dot(normal) + d ) / ray.D.dot(normal);

    if(t<=0){
    	return Hit::NO_HIT();
    }

    Vector N = normal;

    return Hit(t,N);
}
