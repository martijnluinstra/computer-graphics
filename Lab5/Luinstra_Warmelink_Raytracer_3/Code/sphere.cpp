//
//  Framework for a raytracer
//  File: sphere.cpp
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Maarten Everts
//    Jasper van de Gronde
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "sphere.h"
#include <iostream>
#include <math.h>

#define PI 3.14159265

/************************** Sphere **********************************/

Hit Sphere::intersect(const Ray &ray)
{
    /****************************************************
    * RT1.1: INTERSECTION CALCULATION
    *
    * Given: ray, position, r
    * Sought: intersects? if true: *t
    * 
    * Insert calculation of ray/sphere intersection here. 
    *
    * You have the sphere's center (C) and radius (r) as well as
    * the ray's origin (ray.O) and direction (ray.D).
    *
    * If the ray does not intersect the sphere, return false.
    * Otherwise, return true and place the distance of the
    * intersection point from the ray origin in *t (see example).
    ****************************************************/


    double t = 0;

    /* The equation to calculate the intersection between a ray 
     * and a sphere can be simplified to a quadratic equation.
     * First, we calculate the A, B and C components of this equation.
     */

    double a = ray.D.dot(ray.D);
    double b = 2*ray.D.dot(ray.O - position);
    double c = (ray.O - position).length()*(ray.O - position).length() - r*r;

    // Calculate the discriminant of the quadratic formula
    double D = b*b - (4 * a * c);

    // No hit if D<0
    if (D < 0){
        return Hit::NO_HIT();
    }

    // Exactly 1 intersection if D==0
    if (D == 0){
        t = -b / 2*a;
    }

    /* 2 intersections if D > 0
     * only use the closest intersection that is in front of the eye,
     * as the other intersection is at the back of the sphere.
     */
    if (D > 0){
        double t1 = (-b + sqrt(D)) / 2*a;
        double t2 = (-b - sqrt(D)) / 2*a;

        if(t1<0){
            if(t2<0){
                return Hit::NO_HIT();
            }
            t =  t2;
        }else if(t2<0){
            t = t1;
        }else{
            t = min(t1, t2);
        }
    }

    if(t<0.001){
        return Hit::NO_HIT();
    }


    /****************************************************
    * RT1.2: NORMAL CALCULATION
    *
    * Given: t, C, r
    * Sought: N
    * 
    * Insert calculation of the sphere's normal at the intersection point.
    ***************************************************/

    // (point on the circle - center) / radius
    Vector N = ((ray.at(t)-position)/r).normalized();

    return Hit(t,N);
}

Color Sphere::getTextureColor(const Point &hit)
{
    // First, rotate the point to map the texture correctly
    Point rotated_hit = rotatePoint(hit);

    // Calculate phi
    float phi = atan2(rotated_hit.y - position.y, rotated_hit.x - position.x);
    if(phi < 0){
        phi += 2*PI;
    }

    // Calculate texture coordinates
    float u = phi / (2 * M_PI);
    float v = (M_PI - acos( (rotated_hit.z - position.z) / r) ) / M_PI ;
    
    // Return color according to texture
    return material->texture->colorAt(u, v);
}

Point Sphere::rotatePoint(const Point &point){
    // No rotation implemented
    return point;
}
