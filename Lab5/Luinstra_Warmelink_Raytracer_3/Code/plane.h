#ifndef PLANE_H_INCLUDED
#define PLANE_H_INCLUDED

#include "object.h"

class Plane : public Object
{
public:
    Plane(Point pos, Triple normal) : pos(pos), normal(normal) { }

    virtual Hit intersect(const Ray &ray);
    virtual Color getTextureColor(const Point &hit);
    virtual Point rotatePoint(const Point &point);


    const Point pos;
    const Triple normal;
  
};

#endif /* end of include guard: */