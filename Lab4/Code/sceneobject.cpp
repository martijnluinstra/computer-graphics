#include "sceneobject.h"
#include <math.h>


SceneObject::SceneObject()
{
    init();
}

SceneObject::SceneObject(QVector3D location, double scale, QOpenGLTexture* texture, QQuaternion rotation, QVector3D orbit_center, double orbit_speed, QVector3D orbit_normal) : location(location), scale(scale), texture(texture), rotation(rotation), orbit_center(orbit_center), orbit_speed(orbit_speed), orbit_normal(orbit_normal)
{
    init();
}

void SceneObject::init()
{
    // Intitialize the variables used to remember the position in orbit and spin
    orbit_position = QQuaternion(0, orbit_normal);
    rotation_offset = QQuaternion();
}

void SceneObject::update()
{
    // Calculate spin
    rotation_offset *= rotation * rotation;
    rotation_offset.normalize();

    // Calculate a matrix to orbit the object
    QMatrix4x4 modelMatrix;
    modelMatrix.setToIdentity();
    modelMatrix.translate(orbit_center);
    modelMatrix.rotate(QQuaternion(orbit_speed, orbit_normal)*QQuaternion(orbit_speed, orbit_normal));
    modelMatrix.translate(-1 * orbit_center);

    // Calculate new location in orbit;
    location = modelMatrix * location;
}
