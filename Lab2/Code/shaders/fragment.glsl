#version 330
// Specify the inputs to the fragment shader
in vec4 gl_Color;

// Specify the Uniforms of the vertex shaders
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

// Specify the output of the fragment shader
// Usually a vec4 describing a color
out vec4 fColor;

void main()
{
    // Plain white
    fColor = gl_Color;
}
